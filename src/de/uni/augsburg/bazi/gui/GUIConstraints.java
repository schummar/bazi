/*
 * @(#)GUIConstraints.java 2.1 07/04/11
 * 
 * Copyright (c) 2000-2007 Lehrstuhl für Stochastik und ihre Anwendungen
 * Institut für Mathematik, Universität Augsburg
 * D-86135 Augsburg, Germany
 */


package de.uni.augsburg.bazi.gui;

import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;

/** <b>Title:</b> Klasse GUIConstraints<br>
 * <b>Description:</b> Enthält die Objekte für ein einheitliches L&F<br>
 * <b>Copyright:</b> Copyright (c) 2000-2007<br>
 * <b>Company:</b> Universität Augsburg
 * 
 * @version 2.1
 * @author Florian Kluge, Christian Brand */
public class GUIConstraints
{

	// Schriftarten
	/** Fette Schrift für Label, die Überschriften darstellen. */
	public static final FontUIResource boldFont = new FontUIResource("SansSerif",
			Font.BOLD, 12);

	/** Normale Schrift für Label, die keine Überschriften darstellen. */
	public static final FontUIResource plainFont = new FontUIResource("SansSerif",
			Font.PLAIN, 12);

	/** Kursive Schrift */
	public static final FontUIResource italicFont = new FontUIResource("SansSerif",
			Font.ITALIC, 12);

	/** Kursive, fette Schrift */
	public static final FontUIResource italicBoldFont = new FontUIResource("SansSerif",
			Font.ITALIC + Font.BOLD, 12);

	/** Monospace Schrift */
	public static final FontUIResource monospaceFont = new FontUIResource("Monospaced",
			Font.PLAIN, 12);

	/** Standard-Konstruktor */
	public GUIConstraints()
	{}

	/** Main-Methode nur zum Testen */
	public static void main(String[] argv)
	{
		try
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			// UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
			// UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		}
		catch (Exception e)
		{
			System.out.println(e);
		}

		System.out.println("PF: " + plainFont);
		System.out.println("BF: " + boldFont);
		JTable jt = new JTable();
		FontUIResource f = new FontUIResource(jt.getFont());
		System.out.println("JT: " + f);
		JMenu jm = new JMenu();
		f = (FontUIResource) jm.getFont();
		System.out.println("JM: " + f);
		JMenuItem jmi = new JMenuItem();
		f = (FontUIResource) jmi.getFont();
		System.out.println("MI: " + f);
		JLabel jl = new JLabel();
		f = (FontUIResource) jl.getFont();
		System.out.println("JL: " + f);
		JTextField jtf = new JTextField();
		f = (FontUIResource) jtf.getFont();
		System.out.println("TF: " + f);
	}
}
