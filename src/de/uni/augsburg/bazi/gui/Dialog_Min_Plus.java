package de.uni.augsburg.bazi.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import de.uni.augsburg.bazi.Resource;

public class Dialog_Min_Plus extends JDialog implements ActionListener
{
	private static final long serialVersionUID = 1L;

	private JTextField textField;
	private double value;

	public Dialog_Min_Plus(RoundFrame rf, double value)
	{
		super(rf, Resource.getString("bazi.gui.menu.minplus"), true);
		setLocationRelativeTo(rf);
		this.value = value;

		JPanel panel = new JPanel();
		panel.add(new JLabel(Resource.getString("bazi.gui.minplus.desc")));
		add(panel, BorderLayout.NORTH);

		panel = new JPanel();
		panel.add(textField = new JTextField(10));
		textField.setText(value + "");
		panel.add(new JLabel("%"), BorderLayout.EAST);
		add(panel);

		panel = new JPanel();
		JButton b = new JButton(Resource.getString("bazi.gui.ok"));
		b.addActionListener(this);
		panel.add(b);
		b = new JButton(Resource.getString("bazi.gui.cancel"));
		b.addActionListener(this);
		panel.add(b);
		add(panel, BorderLayout.SOUTH);

		pack();
		setVisible(true);
	}

	public double getValue()
	{
		return value;
	}

	public void actionPerformed(ActionEvent e)
	{
		if (e.getActionCommand().equals(Resource.getString("bazi.gui.ok")))
			try
			{
				value = Double.parseDouble(textField.getText());
			}
			catch (Exception ex)
			{}
		dispose();
	}
}
