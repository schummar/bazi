/*
 * @(#)ChangelogDialog.java 10/04/2011
 * 
 * Copyright (c) 2000-2011 Lehrstuhl für Stochastik und ihre Anwendungen
 * Institut für Mathematik, Universität Augsburg
 * D-86135 Augsburg, Germany
 */


package de.uni.augsburg.bazi.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import de.uni.augsburg.bazi.Resource;

/** <b>Title:</b> Klasse ChangelogDialog<br>
 * <b>Description:</b> Anzeigen der letzten Änderungen<br>
 * <b>Copyright:</b> Copyright (c) 2000-2011<br>
 * <b>Company:</b> Universität Augsburg<br>
 * 
 * @author Marco Schumacher */
public class ChangelogDialog extends JDialog
{
	/** Default UID */
	private static final long serialVersionUID = 1L;

	/** Erzeugt eine Instanz von ChangelogDialog.
	 * 
	 * @param roundFrame Referenz auf das RoundFrame. */
	public ChangelogDialog(final RoundFrame roundFrame)
	{
		super(roundFrame, Resource.getString("bazi.gui.menu.changelog"), true);

		JPanel pMain = new JPanel(new BorderLayout());

		JEditorPane jepChl = new JEditorPane("text/html", getText());
		jepChl.setFont(new Font("Monospaced", 0, 12));
		jepChl.setCaretPosition(0);
		jepChl.setEditable(false);
		jepChl.setBackground(pMain.getBackground());
		jepChl.addHyperlinkListener(new HyperlinkListener()
		{
			public void hyperlinkUpdate(HyperlinkEvent e)
			{
				if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED)
				{
					roundFrame.showDocument(e.getDescription());
				}
			}
		});
		int w = Integer.parseInt(Resource.getString("bazi.gui.info.width"));
		int h = Integer.parseInt(Resource.getString("bazi.gui.info.height"));
		jepChl.setPreferredSize(new Dimension(w, h));
		jepChl.setRequestFocusEnabled(false);

		pMain.add(new JScrollPane(jepChl));
		setContentPane(pMain);
		pack();
		setLocationRelativeTo(roundFrame);

		setVisible(true);
	}

	private String getText()
	{
		try
		{
			String out = "";
			InputStreamReader in = new InputStreamReader(new FileInputStream("changelog.html"));
			BufferedReader reader = new BufferedReader(in);
			String temp;
			while ((temp = reader.readLine()) != null)
			{
				out += temp + "\n";
			}
			reader.close();
			in.close();
			return out;
		}
		catch (IOException e)
		{
			System.out.println("IO Error:" + e.getMessage());
		}
		return "Not found: changelog.html";
	}
}
