package de.uni.augsburg.bazi;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import de.uni.augsburg.bazi.gui.InfoFrame;
import de.uni.augsburg.bazi.gui.RoundFrame;
import de.uni.augsburg.bazi.lib.BrowserLaunch;


/** <b>Title:</b> Klasse VersionControl<br>
 * <b>Description:</b> Zum Überprüfen, ob eine neue Version verfügbar ist<br>
 * <b>Copyright:</b> Copyright (c) 2000-20010<br>
 * <b>Company:</b> Universität Augsburg<br>
 * 
 * @version 2011-01-06
 * @author Marco Schumacher */
public class VersionControl implements Runnable
{
	/** Jahr für Copyright: 2002-YEAR */
	public static final String YEAR = "2014";

	/** aktuelle Versionsnummer */
	private static volatile String VERSION = null;

	public static String getVersion()
	{
		if (VERSION != null)
			return VERSION;

		synchronized (VersionControl.class)
		{
			if (VERSION != null)
				return VERSION;

			InputStream stream = null;
			try
			{
				stream = VersionControl.class.getResourceAsStream("version.properties");
				Properties prop = new Properties();
				prop.load(stream);

				VERSION = prop.getProperty("version");
			}
			catch (IOException e)
			{}
			finally
			{
				if (stream != null)
					try
					{
						stream.close();
					}
					catch (IOException e)
					{}
			}
		}

		return VERSION;
	}

	public static String PATTERN_STABLE = "\\d+\\.\\d+",
			PATTERN_BETA = "\\d+\\.\\d+-b-\\d+";


	/** Dieser Thread versucht die Verbindung aufzubauen
	 * und die Versionsnummern abzugleichen */
	private final Thread update_thread;

	/** ob es sich um eine Überprüfung im Hintergrund handelt */
	private final boolean silent;

	/** Nachricht, die ausgegeben werden soll und URL, die aufgerufen werden soll */
	private String message, url;

	/** ob eine neue Version verfügbar ist */
	private boolean available = false;

	/** nicht sichtbarer Konstruktor */
	private VersionControl(boolean silent)
	{
		this.silent = silent;
		update_thread = new Thread(this);
		update_thread.start();

		if (!silent)
			new VersionDialog();
	}

	/** Überprüft und zeigt gleich den Dialog an */
	public static void check()
	{
		new VersionControl(false);
	}

	/** Überprüft und zeigt und zeigt einen Hinweis im InfoFrame
	 * wenn eine neue Version verfügbar ist */
	public static void checkSilent()
	{
		new VersionControl(true);
	}

	/** Wird von update_thread aufgerufen, lädt Infos vom Server und vergleicht die Versionen */
	@Override
	public void run()
	{
		try
		{
			URL url = new URL(Resource.getString("bazi.version.url_check"));
			BufferedReader br = new BufferedReader(new InputStreamReader(url.openConnection().getInputStream()));

			String stable = br.readLine();
			String beta = br.readLine();

			if (!stable.matches(PATTERN_STABLE) || !beta.matches(PATTERN_BETA))
			{
				return;
			}

			if (!checkBeta(beta, stable) && !checkStable(stable))
			{
				message = Resource.getString("bazi.version.not_available");
			}

		}
		catch (Exception e)
		{
			available = false;
			message = Resource.getString("bazi.version.error");
		}

		if (silent && available)
		{
			message = message.replaceAll("<html>|</html>", "").replaceAll("<br>", "\n");
			InfoFrame info = InfoFrame.getInfoFrame();
			info.setText(message);
			info.setVisible(true);
		}
	}

	/** Wenn der User eine Stable-Version besitzt soll er auch nur über neue Stable-Versionen
	 * informiert werden.
	 * 
	 * @param stable Stable-Versionsnummer vom Server
	 * @return <b>true</b> falls eine neue Version verfügbar ist */
	private boolean checkStable(String stable)
	{
		if (compare(stable, getVersion()) > 0)
		{
			available = true;
			url = Resource.getString("bazi.version.url_dl");
			message = Resource.getString("bazi.version.available1")
					+ Resource.getString("bazi.version.your") + getVersion()
					+ Resource.getString("bazi.version.current") + stable;
			if (!silent)
				message = message
						+ Resource.getString("bazi.version.available2")
						+ url
						+ Resource.getString("bazi.version.available3");
			else
				message = message
						+ Resource.getString("bazi.version.availableSilent")
						+ url;
			return true;
		}
		else
		{
			available = false;
			return false;
		}
	}

	/** Wenn der User eine Beta-Version besitzt soll er über neue Beta- und Stable-Versionen
	 * informiert werden.
	 * 
	 * @param beta Beta-Version vom Server
	 * @param stable Stable-Versionsnummer vom Server
	 * @return <b>true</b> falls eine neue Version verfügbar ist */
	private boolean checkBeta(String beta, String stable)
	{
		if (getVersion().indexOf("b") == -1)
			return false;

		String aktueller, akt_url;
		String zusatz;

		if (compare(beta, stable) > 0)
		{
			aktueller = beta;
			akt_url = Resource.getString("bazi.version.url_beta");
			zusatz = " (beta)";
		}
		else
		{
			aktueller = stable;
			akt_url = Resource.getString("bazi.version.url_dl");
			zusatz = " (stable)";
		}

		if (compare(aktueller, getVersion()) > 0)
		{
			available = true;
			url = akt_url;
			message = Resource.getString("bazi.version.available1")
					+ Resource.getString("bazi.version.your") + getVersion() + " (beta)"
					+ Resource.getString("bazi.version.current") + aktueller + zusatz;
			if (!silent)
				message = message
						+ Resource.getString("bazi.version.available2")
						+ url
						+ Resource.getString("bazi.version.available3");
			else
				message = message
						+ Resource.getString("bazi.version.availableSilent")
						+ url;
			return true;
		}
		else
		{
			available = false;
			return false;
		}
	}


	/** Vergleich zwei Versionsnummern miteinander
	 * 
	 * @param o1 Versionsnummer 1
	 * @param o2 Versionsnummer 2
	 * @return <b>0</b> wenn o1 und o2 identisch sind<br>
	 *         <b>+1</b> wenn o1 neuer ist als o2<br>
	 *         <b>-1</b> wenn o1 älter ist als o2 */
	public static int compare(String o1, String o2)
	{
		int temp = o1.split("\\.")[0].compareTo(o2.split("\\.")[0]);
		if (temp != 0)
			return temp;

		temp = o1.split("\\.|-")[1].compareTo(o2.split("\\.|-")[1]);
		if (temp != 0)
			return temp;

		if (o1.indexOf("b") == -1 && o2.indexOf("b") != -1)
			return 1;
		else if (o1.indexOf("b") != -1 && o2.indexOf("b") == -1)
			return -1;
		else if (o1.indexOf("b") != -1 && o2.indexOf("b") != -1)
		{
			temp = o1.split("-")[2].compareTo(o2.split("-")[2]);
			return temp;
		}

		return 0;
	}

	/** öffnet eine URL im Standardbrowser
	 * 
	 * @param url die URL, die aufgerufen werden soll */
	private void goURL(String url)
	{
		BrowserLaunch.openURL(url);
	}


	/** <b>Title:</b> Klasse VersionDialog<br>
	 * <b>Description:</b> Dialog, der das Ergebnis der Versionssuche ausgibt<br>
	 * <b>Copyright:</b> Copyright (c) 2000-20010<br>
	 * <b>Company:</b> Universität Augsburg<br>
	 * 
	 * @version 2010-11-14
	 * @author Marco Schumacher */
	private class VersionDialog extends JDialog implements Runnable, ActionListener
	{
		/** Defaul UID */
		private static final long serialVersionUID = 1L;

		private final JPanel container = new JPanel();
		private final JLabel label = new JLabel();

		private int dots = 0;
		private final int maxdots = 3;

		private double time;


		/** Erzeugt einen VersionDialog und öffnet ihn, wenn silent == false ist */
		public VersionDialog()
		{
			super(RoundFrame.getRoundFrame(), Resource.getString("bazi.gui.menu.update"));
			// Probleme auf den Mac 'method is undefined'
			// setIconImage(RoundFrame.getRoundFrame().getIconImage());

			BorderLayout bl = new BorderLayout();
			bl.setVgap(10);
			container.setLayout(bl);

			container.setBorder(new EmptyBorder(10, 10, 10, 10));
			container.add(label);

			add(container);
			pack();
			setResizable(false);

			new Thread(this).start();

			if (!silent)
			{
				reposition();
				open();
			}
		}

		private void open()
		{
			setModal(true);
			setVisible(true);
		}

		private void reposition()
		{
			RoundFrame rf = RoundFrame.getRoundFrame();
			int posX = rf.getLocation().x + (rf.getSize().width - getSize().width) / 2;
			int posY = rf.getLocation().y + (rf.getSize().height - getSize().height) / 2;
			setLocation(posX, posY);
		}

		@Override
		public void run()
		{
			while (update_thread.isAlive())
			{
				String msg = Resource.getString("bazi.version.loading");
				for (int i = 0; i <= dots; i++)
					msg += ".";
				label.setText(msg);

				dots = (dots + 1) % (maxdots + 1);

				if (time > 5000)
				{
					label.setText(Resource.getString("bazi.version.error"));
					return;
				}

				try
				{
					Thread.sleep(200);
					time += 200;
				}
				catch (InterruptedException e)
				{}
			}

			fill();

			if (silent && available)
			{
				reposition();
				open();
			}
		}

		private void fill()
		{
			container.removeAll();

			container.add(new JLabel(message));

			JPanel panel = new JPanel();

			JButton button = new JButton(Resource.getString("bazi.gui.ok"));
			button.setPreferredSize(new Dimension(100, 25));
			button.addActionListener(this);
			panel.add(button);

			if (available)
			{
				button = new JButton(Resource.getString("bazi.gui.cancel"));
				button.setPreferredSize(new Dimension(100, 25));
				button.addActionListener(this);
				panel.add(button);
			}

			container.add(panel, BorderLayout.SOUTH);

			container.updateUI();
			pack();
		}


		@Override
		public void actionPerformed(ActionEvent e)
		{
			if (e.getActionCommand().equals(Resource.getString("bazi.gui.ok")) && available)
			{
				goURL(url);
				setVisible(false);
				dispose();
			}
			else
			{
				setVisible(false);
				dispose();
			}
		}
	}
}
